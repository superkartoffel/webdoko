class DokoGame {

    constructor(app) {
        this.compareCard = this.compareCard.bind(this);

        const shuffledDeck = this.shuffleCards(this.createDeckWithoutNine());
        
        this.app = app;
        this.cardsPerPlayer = shuffledDeck.length / 4;
        this.gameState = {
            hands: [
                shuffledDeck.slice( 0,this.cardsPerPlayer), 
                shuffledDeck.slice(this.cardsPerPlayer,2*this.cardsPerPlayer),
                shuffledDeck.slice(2*this.cardsPerPlayer,3*this.cardsPerPlayer),
                shuffledDeck.slice(3*this.cardsPerPlayer,4*this.cardsPerPlayer),
            ],
            teams: [null, null, null, null],
            pips: [0,0,0,0],
            announcements: [],
            possibleAnnouncements: [
                [],
                [],
                [],
                [],
            ],

            currentTrick: this.createNewTrick(),
            tricks: [],
            clarificationTrick: 0,

            currentPlayerIndex: null,
            gameMode: null,
            obligatorySoloPlayer: null,

            extraPoints: [],
            winner: null,
            pointReceiver: null,
            value: null,
            points: [],
        };

        this.players = [
            null,
            null, 
            null, 
            null,
        ];


        // trigger bots to make an announcement
        this.calculatePossibleAnnouncements();        
        this.reassignTrumpCards();
        this.reoderHandCards();

    this.setGameResults = this.setGameResults.bind(this);
    }

    setGameResults(gameResults) {
        this.gameResults = gameResults;
    }

    createAnnouncement(name, player) {
        const announcementTypes = {
            healthy: "playMode",
            marriage: "playMode",
            jackSolo: "playMode",
            queenSolo: "playMode",
            diamondsSolo: "playMode",
            heartsSolo: "playMode",
            spadesSolo: "playMode",
            clubsSolo: "playMode",
            meatlessSolo: "playMode",
            swine: "card",
            superswine: "card",
            hyperswine: "card",
            re: "team",
            contra: "team",
            reWillHaveNinetyOrLess: "value",
            reWillHaveSixtyOrLess: "value",
            reWillHaveThirtyOrLess: "value",
            reWillBeBlank: "value",
            contraWillHaveNinetyOrLess: "value",
            contraWillHaveSixtyOrLess: "value",
            contraWillHaveThirtyOrLess: "value",
            contraWillBeBlank: "value",
        };

        if (!announcementTypes.hasOwnProperty(name) || typeof player !== "number" || player < 0 || player > 3) {
            return undefined;
        }

        return {
            name: name,
            player: player,
            type: announcementTypes[name],
            trickNumber: this.gameState.tricks.length,
        }
    }

    createCard(shortName, id) {
        const colors = {
          s: "♠",
          h: "♥",
          d: "♦",
          c: "♣"
        };

        const pointValues = {
            "9": 0,
            "10": 10,
            "J": 2,
            "Q": 3,
            "K": 4,
            "A": 11,
        }

        return {
            id: id,
            color: colors[shortName[0]],
            value: shortName.substr(1),
            shortName: shortName,
            displayName: colors[shortName[0]] + shortName.substr(1),
            canDrawNow: false,
            pips: pointValues[shortName.substr(1)],
            isSwine: false,
            isSuperSwine: false,
            isHyperSwine: false,
            isTrump: false,
        };
    }

    createDeck(cardShortNames) {
        return cardShortNames.map((shortName, index) => this.createCard(shortName, index));
    }

    createDefaultDeck() {
        return this.createDeck([
            "d9", "d10", "dJ", "dQ", "dK", "dA",
            "d9", "d10", "dJ", "dQ", "dK", "dA",
            "h9", "h10", "hJ", "hQ", "hK", "hA",
            "h9", "h10", "hJ", "hQ", "hK", "hA",
            "s9", "s10", "sJ", "sQ", "sK", "sA",
            "s9", "s10", "sJ", "sQ", "sK", "sA",
            "c9", "c10", "cJ", "cQ", "cK", "cA",
            "c9", "c10", "cJ", "cQ", "cK", "cA",
        ]);
    }

    createDeckWithoutNine() {
        return this.createDeck([
            "d10", "dJ", "dQ", "dK", "dA",
            "d10", "dJ", "dQ", "dK", "dA",
            "h10", "hJ", "hQ", "hK", "hA",
            "h10", "hJ", "hQ", "hK", "hA",
            "s10", "sJ", "sQ", "sK", "sA",
            "s10", "sJ", "sQ", "sK", "sA",
            "c10", "cJ", "cQ", "cK", "cA",
            "c10", "cJ", "cQ", "cK", "cA",
        ]);
    }

    createNewTrick() {
        return {
            cards: [],
            color: null,
            isTrump: null,
            firstCardIndex: null,
        };
    }

    createExtraPoint(type, winningPlayer, fromPlayer) {
        return {
            type: type, // full, fox, karl
            winningPlayer: winningPlayer,
            fromPlayer: fromPlayer,
        }
    }

    createGameResult(points, value, gameMode, obligatorySoloPlayer) {
        return {
            player1points: points[0],
            player2points: points[1],
            player3points: points[2],
            player4points: points[3],
            value: value,
            gameMode: gameMode,
            obligatorySoloPlayer: obligatorySoloPlayer, 
        };
    }

    isTrickFinished() {
        if (this.gameState.currentTrick.cards.length !== 4) {
            return false;
        }
        for (let i=0; i<4; i++) {
            if (this.gameState.currentTrick.cards[i] === undefined) {
                return false;
            }
        }
        
        return true;
    }

    getTrickWinner() {
        if (!this.isTrickFinished()) {
            return; // not all players played a card
        }

        let firstCardPlayer = (this.gameState.currentPlayerIndex + 1) % 4;
        let highestCard = this.gameState.currentTrick.cards[firstCardPlayer];
        let winningPlayerIndex = firstCardPlayer;
        for (let i=1;i<4;i++) {
            const currentCardIndex = (i + firstCardPlayer) % 4;
            const otherCard = this.gameState.currentTrick.cards[currentCardIndex];
            if (this.gameState.currentTrick.isTrump) {
                if (!otherCard.isTrump) {
                    continue;
                }
                else if (this.compareCard(highestCard, otherCard) > 0) {
                    winningPlayerIndex = currentCardIndex;
                    highestCard = otherCard;
                }
            }
            else {
                if (!otherCard.isTrump && this.gameState.currentTrick.color !== otherCard.color) {
                    continue;
                }
                else if (this.compareCard(highestCard, otherCard) > 0) {
                    winningPlayerIndex = currentCardIndex;
                    highestCard = otherCard;
                }
            }
        }
        return winningPlayerIndex;
    }

    compareCard(thisCard, otherCard) {
        return this.calculateCardOrderValue(thisCard) - this.calculateCardOrderValue(otherCard);
    }

    calculateCardOrderValue(card) {
        const colorValues = {
            "♣": 50,
            "♠": 40,
            "♥": 30,
            "♦": 20,
        };
        return -( card.pips /* 0-11 */
                + colorValues[card.color] /* 20-50 */
                + 100 * card.isTrump 
                + 200 * (card.isTrump && card.value === "Q")
                + 100 * (card.isTrump && card.value === "J")
                + 300 * (card.isTrump && card.shortName === "h10")
                + 400 * card.isSwine 
                + 500 * card.isSuperSwine
                + 600 * card.isHyperSwine 
                );
    }

    shuffleCards(deck) {
        // Fisher–Yates shuffle taken from https://bost.ocks.org/mike/shuffle/
        var m = deck.length, t, i;
        while (m) {
          i = Math.floor(Math.random() * m--);
          t = deck[m];
          deck[m] = deck[i];
          deck[i] = t;
        }
      
        return deck;
    }

    isQuietMarriage() {
        // marriage without partner
        if (this.gameState.gameMode === "marriage") {
            return this.gameState.tricks.length > 2;
        }
        
        // actual quiet marriage
        let firstRe=-1;
        for (let trick of this.gameState.tricks) {
            for (let i=0;i<trick.cards.length;i++) {
                if (trick.cards[i].shortName === "cQ") {
                    firstRe = i;
                    break;
                }
            }
        }
        for (let i=0;i<this.gameState.currentTrick.cards.length;i++) {
            if (this.gameState.currentTrick.cards[i] && this.gameState.currentTrick.cards[i].shortName === "cQ") {
                return firstRe === i;
            }
        }
        return false;
    }

    setPlayersTeam(playerIndex, team) {
        if (this.gameState.gameMode === "normal" || this.gameState.gameMode === "marriage") {
            let bQuietMarriage = this.isQuietMarriage();

            if (bQuietMarriage) {
                this.gameState.gameMode = "diamondsSolo";
            }

            this.gameState.teams[playerIndex] = team;

            let numRe = 0, numContra = 0;
            for (let i=0;i<this.gameState.teams.length; i++) {
                numRe += (this.gameState.teams[i] === "re") ? 1 : 0;
                numContra += (this.gameState.teams[i] === "contra") ? 1 : 0;
            }
            if (numRe === 2 || numContra === 2 || bQuietMarriage) {
                for (let i=0;i<this.gameState.teams.length; i++) {
                    if (this.gameState.teams[i] === null) {
                        this.gameState.teams[i] = (numRe === 2 || bQuietMarriage) ? "contra" : "re";
                    }
                }
            }
        }
        else { // solo
            for (let i=0;i<this.gameState.teams.length; i++) {
                this.gameState.teams[i] = (i===playerIndex) ? "re" : "contra";
            }
        }
    }

    moveHandCardToTable(hand, id) {
        let thisCard = this.gameState.hands[hand].filter((card) => { return card.id === id; })[0];
        
        if (this.gameState.currentPlayerIndex === hand && thisCard.canDrawNow) {
            if (this.gameState.currentTrick.cards.length === 0) {
                this.gameState.currentTrick.color = thisCard.color;
                this.gameState.currentTrick.isTrump = thisCard.isTrump;
                this.gameState.currentTrick.firstCardIndex = hand;
            }

            this.gameState.currentTrick.cards[hand] = thisCard;
            this.gameState.hands[hand] = this.gameState.hands[hand].filter((card) => { return card.id !== id; });

            if (this.gameState.gameMode === "normal" && thisCard.shortName === "cQ") {
                this.setPlayersTeam(hand, "re");
            }

            this.nextPlayer();
            return true;
        }
        return false;
    }

    nextPlayer() {
        if (this.isTrickFinished()) {
            console.log("timer => nextTrick()");
            setTimeout(() => { this.nextTrick(); }, 2000);
            return;
        }
        
        if (this.gameState.currentPlayerIndex === null || this.gameState.currentPlayerIndex === undefined) {
            this.gameState.currentPlayerIndex = this.calculateStartPlayer();
        }
        else  {
            this.gameState.currentPlayerIndex = (this.gameState.currentPlayerIndex + 1) % 4;
        }

        // check new current player has cards on his hand. if not, the game is over
        if (this.getCurrentPlayersHand().length === 0) {
            this.gameState.currentPlayerIndex = null;
            this.calculateWinner();
            this.calculateGameValue();
            this.calculateGameResult();
        }

        this.calculatePlayableCards();
        this.calculatePossibleAnnouncements();

        let currentPlayer = this.getCurrentPlayer();
        if (currentPlayer && typeof(currentPlayer.myTurn) == "function") {
            this.getCurrentPlayer().myTurn(this, this.gameState.currentPlayerIndex);
        }
    }

    nextTrick() {
        // calculate who won the trick
        let winningPlayerIndex = this.getTrickWinner();
        console.log("winner: " + winningPlayerIndex);
        if (winningPlayerIndex !== undefined) {
            this.gameState.currentTrick.winner = winningPlayerIndex;
            this.gameState.currentTrick.value = 0;
            for (let card of this.gameState.currentTrick.cards) { 
                this.gameState.currentTrick.value += card.pips;
            }
            this.gameState.pips[winningPlayerIndex] += this.gameState.currentTrick.value;

            // check for extrapoints
            if (this.gameState.gameMode === "normal") {
                // full
                if (this.gameState.currentTrick.value >= 40) {
                    this.gameState.extraPoints.push(this.createExtraPoint("full", winningPlayerIndex));
                }
                for (let i=0;i<4;i++) { 
                    // fox
                    if (this.gameState.currentTrick.cards[i].shortName === "dA") {
                        this.gameState.extraPoints.push(this.createExtraPoint("fox", winningPlayerIndex, i));
                    }
                    // karl
                    if (this.getCurrentPlayersHand().length === 0 && 
                        this.gameState.currentTrick.cards[i].shortName === "cJ") {
                        this.gameState.extraPoints.push(this.createExtraPoint("karl", winningPlayerIndex, i));
                    }
                }
            }
            // check for clarification in marriage
            if (this.gameState.clarificationTrick === null /*&& this.gameState.gameMode === "marriage"*/) {
                let bride = this.gameState.announcements.filter(a => a.name === "marriage")[0].player;
                if (this.gameState.tricks.length === 2 || winningPlayerIndex !== bride) {
                    this.gameState.clarificationTrick = this.gameState.tricks.length;
                    this.setPlayersTeam(winningPlayerIndex, "re");
                }
            }

            this.gameState.currentPlayerIndex = winningPlayerIndex-1;
        }
        else {
            console.log("error while calculating winner");
        }

        // reset trick
        this.gameState.tricks.push(this.gameState.currentTrick);
        this.gameState.currentTrick = this.createNewTrick();

        this.nextPlayer();
        this.app.updateState();
    }

    makeAnnouncement(announcementName, player) {
        let announcement = this.createAnnouncement(announcementName, player);
        if (announcement !== undefined) {
            this.gameState.announcements.push(announcement);

            if (announcement.type === "playMode") {
                let caveats = this.gameState.announcements.filter(entry => entry.type === "playMode");
                if (caveats.length === 4)
                {
                    caveats.sort((a,b) => a.player - b.player);
                    let bObligatorySolo = false;
                    let marriagePlayer = -1;
                    let soloPlayer = -1;
                    let getObligatory = function(app) { return app.gameResults.filter(e => e.obligatorySoloPlayer === soloPlayer).length === 0 };
                    for (let i=0;i<caveats.length;i++) {
                        if (caveats[i].name !== "healthy") {
                            if (caveats[i].name === "marriage") {
                                marriagePlayer = i;
                            }
                            else {
                                soloPlayer = i;
                                bObligatorySolo = getObligatory(this);
                                if (bObligatorySolo) {
                                    break;
                                }
                            }
                        }
                    }
                    if (marriagePlayer !== -1 && soloPlayer === -1) {
                        soloPlayer = marriagePlayer;
                        bObligatorySolo = false;
                        this.gameState.clarificationTrick = null;
                    }

                    // solo?
                    if (soloPlayer !== -1) {
                        this.gameState.gameMode = caveats[soloPlayer].name;
                        this.setPlayersTeam(soloPlayer, "re");
                        if (bObligatorySolo) {
                            this.gameState.currentPlayerIndex = soloPlayer-1;
                            this.gameState.obligatorySoloPlayer = bObligatorySolo ? soloPlayer : null;
                        }
                    }
                    else {
                        this.gameState.gameMode = "normal";
                    }
                    
                    this.reassignTrumpCards();
                    this.calculatePlayableCards();
                    this.nextPlayer();
                }
            }

            if (announcement.type === "team") {
                this.setPlayersTeam(announcement.player, announcement.name);
            }

            if (announcement.type === "playMode" 
                || announcement.type === "card") {
                this.reoderHandCards();
            }
            this.calculatePossibleAnnouncements();
        }
    }

    reoderHandCards() {
        for (let i=0;i<this.gameState.hands.length; i++) {
            this.gameState.hands[i].sort(this.compareCard);
        }
    }

    reassignTrumpCards() {
        for (let i = 0; i < this.gameState.hands.length; i++)
        {
            for (let card of this.gameState.hands[i])
            {
                if (this.gameState.gameMode === "normal" || 
                    this.gameState.gameMode === "diamondsSolo" ||
                    this.gameState.gameMode === null) {
                    card.isTrump = (
                        (card.value === "J") ||
                        (card.value === "Q") ||
                        (card.color === "♦") ||
                        (card.color === "♥" && card.value === "10")
                    );
                } 
                else if (this.gameState.gameMode === "hearthsSolo") {
                    card.isTrump = (
                        (card.value === "J") ||
                        (card.value === "Q") ||
                        (card.color === "♥") ||
                        (card.color === "♥" && card.value === "10")
                    );
                } 
                else if (this.gameState.gameMode === "spadesSolo") {
                    card.isTrump = (
                        (card.value === "J") ||
                        (card.value === "Q") ||
                        (card.color === "♠") ||
                        (card.color === "♥" && card.value === "10")
                    );
                } 
                else if (this.gameState.gameMode === "clubsSolo") {
                    card.isTrump = (
                        (card.value === "J") ||
                        (card.value === "Q") ||
                        (card.color === "♣") ||
                        (card.color === "♥" && card.value === "10")
                    );
                } 
                else if (this.gameState.gameMode === "jackSolo") {
                    card.isTrump = (card.value === "J");
                }
                else if (this.gameState.gameMode === "queenSolo") {
                    card.isTrump = (card.value === "Q");
                }
                else if (this.gameState.gameMode === "meatlessSolo") {
                    card.isTrump = false;
                }
            }
        }
    }

    calculatePlayableCards() {
        let preAnnouncmentsDone = this.gameState.announcements.filter((a) => a.type === "playMode").length === 4;
        for (let i = 0; i < this.gameState.hands.length; i++)
        {
            for (let card of this.gameState.hands[i])
            {
                if (i !== this.gameState.currentPlayerIndex || !preAnnouncmentsDone) {
                    // not current player
                    card.canDrawNow = false;
                }
                else if (!this.isTrickFinished()) {
                    let hasAnyTrump = this.getCurrentPlayersHand().some((handCard) => { 
                        return handCard.isTrump;
                    });
                    if (this.gameState.currentTrick.isTrump) {
                        card.canDrawNow = card.isTrump || !hasAnyTrump;
                    } 
                    else {
                        let bMustServe = this.getCurrentPlayersHand().some((handCard) => { 
                            return (!handCard.isTrump && handCard.color === this.gameState.currentTrick.color); 
                        });
                        if (!bMustServe) {
                            card.canDrawNow = true;
                        } 
                        else {
                            card.canDrawNow = (!card.isTrump && card.color === this.gameState.currentTrick.color);
                        }
                    }
                }
            }
        }
    }

    calculatePossibleAnnouncements() {
        for (let i = 0; i < this.gameState.possibleAnnouncements.length; i++) {
            let possibleAnnouncements = [];
            let previousAnnouncements = this.gameState.announcements.filter((a) => a.player === i);
            let allPrevAnnouncmnts = this.gameState.announcements.map(entry => entry.name);
            if (previousAnnouncements.length === 0) {
                possibleAnnouncements = [
                    "healthy",
                    "jackSolo",
                    "queenSolo",
                    "diamondsSolo",
                    "heartsSolo",
                    "spadesSolo",
                    "clubsSolo",
                    "meatlessSolo"];
                    
                    if (this.gameState.hands[i].filter(card => card.shortName === "cQ").length === 2) {
                        possibleAnnouncements.unshift("marriage");
                    }
            }
            else {
                let totalHandCards = this.cardsPerPlayer;
                let playerTeam = this.gameState.teams[i];
                if (playerTeam === null && this.gameState.gameMode === "normal") {
                    playerTeam = (this.gameState.hands[i].filter((card) => { return card.shortName === "cQ"; }).length > 0) ? "re" : "contra";
                }
                // let announcementsCriteria = {
                //     re: 1,
                //     contra: 1,
                //     reWillHaveNinetyOrLess: 2,
                //     reWillHaveSixtyOrLess: 3,
                //     reWillHaveThirtyOrLess: 4,
                //     reWillBeBlank: 5,
                //     contraWillHaveNinetyOrLess: 2,
                //     contraWillHaveSixtyOrLess: 3,
                //     contraWillHaveThirtyOrLess: 4,
                //     contraWillBeBlank: 5,
                // };
                let numHandCards = this.gameState.hands[i].length + this.gameState.clarificationTrick;

                if (playerTeam === "re") {
                    if ((totalHandCards - numHandCards <= 1 ||
                        (totalHandCards - numHandCards <= 2 && allPrevAnnouncmnts.includes("contra")))
                        && !allPrevAnnouncmnts.includes("re")) {
                        possibleAnnouncements.push("re");
                        possibleAnnouncements.push("contraWillHaveNinetyOrLess");   
                        possibleAnnouncements.push("contraWillHaveSixtyOrLess");   
                        possibleAnnouncements.push("contraWillHaveThirtyOrLess");
                        possibleAnnouncements.push("contraWillBeBlank");
                    }
                    if (totalHandCards - numHandCards <= 2 && allPrevAnnouncmnts.includes("re")
                        && !allPrevAnnouncmnts.includes("contraWillHaveNinetyOrLess")) {
                        possibleAnnouncements.push("contraWillHaveNinetyOrLess");   
                        possibleAnnouncements.push("contraWillHaveSixtyOrLess");   
                        possibleAnnouncements.push("contraWillHaveThirtyOrLess");
                        possibleAnnouncements.push("contraWillBeBlank");
                    }
                    if (totalHandCards - numHandCards <= 3 && allPrevAnnouncmnts.includes("contraWillHaveNinetyOrLess")
                        && !allPrevAnnouncmnts.includes("contraWillHaveSixtyOrLess")) {
                        possibleAnnouncements.push("contraWillHaveSixtyOrLess");   
                        possibleAnnouncements.push("contraWillHaveThirtyOrLess");
                        possibleAnnouncements.push("contraWillBeBlank");
                    }
                    if (totalHandCards - numHandCards <= 4 && allPrevAnnouncmnts.includes("contraWillHaveSixtyOrLess")
                        && !allPrevAnnouncmnts.includes("contraWillHaveThirtyOrLess")) {
                        possibleAnnouncements.push("contraWillHaveThirtyOrLess");
                        possibleAnnouncements.push("contraWillBeBlank");
                    }
                    if (totalHandCards - numHandCards <= 5 && allPrevAnnouncmnts.includes("contraWillHaveThirtyOrLess")
                        && !allPrevAnnouncmnts.includes("contraWillBeBlank")) {
                        possibleAnnouncements.push("contraWillBeBlank");
                    }
                }
                else if (playerTeam === "contra") {
                    if ((totalHandCards - numHandCards <= 1 ||
                        (totalHandCards - numHandCards <= 2 && allPrevAnnouncmnts.includes("re")))
                        && !allPrevAnnouncmnts.includes("contra")) {
                        possibleAnnouncements.push("contra");
                        possibleAnnouncements.push("reWillHaveNinetyOrLess");   
                        possibleAnnouncements.push("reWillHaveSixtyOrLess");   
                        possibleAnnouncements.push("reWillHaveThirtyOrLess");
                        possibleAnnouncements.push("reWillBeBlank");
                    }
                    if (totalHandCards - numHandCards <= 2 && allPrevAnnouncmnts.includes("contra")
                        && !allPrevAnnouncmnts.includes("reWillHaveNinetyOrLess")) {
                        possibleAnnouncements.push("reWillHaveNinetyOrLess");   
                        possibleAnnouncements.push("reWillHaveSixtyOrLess");   
                        possibleAnnouncements.push("reWillHaveThirtyOrLess");
                        possibleAnnouncements.push("reWillBeBlank");
                    }
                    if (totalHandCards - numHandCards <= 3 && allPrevAnnouncmnts.includes("reWillHaveNinetyOrLess")
                        && !allPrevAnnouncmnts.includes("reWillHaveSixtyOrLess")) {
                        possibleAnnouncements.push("reWillHaveSixtyOrLess");   
                        possibleAnnouncements.push("reWillHaveThirtyOrLess");
                        possibleAnnouncements.push("reWillBeBlank");
                    }
                    if (totalHandCards - numHandCards <= 4 && allPrevAnnouncmnts.includes("reWillHaveSixtyOrLess")
                        && !allPrevAnnouncmnts.includes("reWillHaveThirtyOrLess")) {
                        possibleAnnouncements.push("reWillHaveThirtyOrLess");
                        possibleAnnouncements.push("reWillBeBlank");
                    }
                    if (totalHandCards - numHandCards <= 5 && allPrevAnnouncmnts.includes("reWillHaveThirtyOrLess")
                        && !allPrevAnnouncmnts.includes("reWillBeBlank")) {
                        possibleAnnouncements.push("reWillBeBlank");
                    }
                }
            }

            this.gameState.possibleAnnouncements[i] = possibleAnnouncements;
        }
    }

    calculateStartPlayer() {
        return this.gameResults.filter(r => r.obligatorySoloPlayer === null).length % 4;
    }

    calculateGameResult() {
        let points = [0,0,0,0];
        for (let i=0; i<4; i++){
            let bWin = this.gameState.teams[i] === this.gameState.pointReceiver;
            if (this.gameState.gameMode === "normal" || this.gameState.gameMode === "marriage") {
                points[i] = bWin ? this.gameState.value : -this.gameState.value;
            }
            else {
                points[i] = (bWin ? this.gameState.value : -this.gameState.value) * (this.gameState.teams[i] === "re" ? 3 : 1);
            }
        }
        this.gameResults.push(this.createGameResult(points, this.gameState.value, this.gameState.gameMode, this.gameState.obligatorySoloPlayer));
    }

    addGameValue(bCheck, points, text) {
        if (bCheck) {
            this.gameState.points.push({points: points, text: text});
            this.gameState.value += points;
        }
    }

    calculateGameValue() {
        let announcements = this.gameState.announcements.map(entry => entry.name);

        this.gameState.pointReceiver = (this.gameState.winner !== "nobody") ? this.gameState.winner : (this.gameState.rePips > this.gameState.contraPips) ? "re" : "contra";

        // 722a
        this.addGameValue(this.gameState.winner !== "nobody", 1, this.gameState.winner + " won");

        this.addGameValue(this.gameState.rePips < 90 || this.gameState.contraPips < 90, 1, "less than 90");
        this.addGameValue(this.gameState.rePips < 60 || this.gameState.contraPips < 60, 1, "less than 60");
        this.addGameValue(this.gameState.rePips < 30 || this.gameState.contraPips < 30, 1, "less than 30");
        this.addGameValue(this.gameState.rePips === 0 || this.gameState.contraPips === 0, 1, "black");

        if (this.gameState.winner !== "nobody") {
            // 722b
            this.addGameValue(announcements.includes("re"), 2, "re announced");
            this.addGameValue(announcements.includes("contra"), 2, "contra announced");

            // 722c
            this.addGameValue(announcements.includes("contraWillHaveNinetyOrLess"), 1, "re announced less than 90");
            this.addGameValue(announcements.includes("contraWillHaveSixtyOrLess"), 1, "re announced less than 60");
            this.addGameValue(announcements.includes("contraWillHaveThirtyOrLess"), 1, "re announced less than 30");
            this.addGameValue(announcements.includes("contraWillBeBlank"), 1, "re announced black");

            // 722d
            this.addGameValue(announcements.includes("reWillHaveNinetyOrLess"), 1, "contra announced less than 90");
            this.addGameValue(announcements.includes("reWillHaveSixtyOrLess"), 1, "contra announced less than 60");
            this.addGameValue(announcements.includes("reWillHaveThirtyOrLess"), 1, "contra announced less than 30");
            this.addGameValue(announcements.includes("reWillBeBlank"), 1, "contra announced black");
        }

        // 722e
        this.addGameValue(this.gameState.rePips >= 120 && announcements.includes("reWillHaveNinetyOrLess"), 1, "re got 120 against announcment of 90");
        this.addGameValue(this.gameState.rePips >= 90 && announcements.includes("reWillHaveSixtyOrLess"), 1, "re got 90 against announcment of 60");
        this.addGameValue(this.gameState.rePips >= 60 && announcements.includes("reWillHaveThirtyOrLess"), 1, "re got 60 against announcment of 30");
        this.addGameValue(this.gameState.rePips >= 30 && announcements.includes("reWillBeBlank"), 1, "re got 30 against announcment black");

        // 722f
        this.addGameValue(this.gameState.contraPips >= 120 && announcements.includes("contraWillHaveNinetyOrLess"), 1, "contra got 120 against announcement of 90");
        this.addGameValue(this.gameState.contraPips >= 90 && announcements.includes("contraWillHaveSixtyOrLess"), 1, "contra got 90 against announcement of 60");
        this.addGameValue(this.gameState.contraPips >= 60 && announcements.includes("contraWillHaveThirtyOrLess"), 1, "contra got 60 against announcement of 30");
        this.addGameValue(this.gameState.contraPips >= 30 && announcements.includes("contraWillBeBlank"), 1, "contra got 30 against announcement black");

        // 723
        if (this.gameState.gameMode === "normal") {
            for (let i=this.gameState.extraPoints.length-1;i>=0;i--) {
                let extraPoint = this.gameState.extraPoints[i];
                let winningTeam = this.gameState.teams[extraPoint.winningPlayer];
                let sign = winningTeam === this.gameState.pointReceiver ? 1 : -1;
                if (extraPoint.type === "fox") {
                    // was the fox catched?
                    this.addGameValue(winningTeam !== this.gameState.teams[extraPoint.fromPlayer], sign, "catched fox");
                } else
                if (extraPoint.type === "karl") {
                    // did karl make the trick?
                    this.addGameValue(extraPoint.winningPlayer === extraPoint.fromPlayer, sign, "karl");
                } else
                if (extraPoint.type === "full") {
                    this.addGameValue(true, sign, "full trick");
                } else {
                    this.gameState.extraPoints.splice(i, 1);
                }
            }
            //against the olds
            this.addGameValue(this.gameState.winner === "contra", 1, "against the olds");
        }
    }

    calculateWinner() {
        this.gameState.rePips = 0;
        this.gameState.contraPips = 0;

        for (let i=0; i<4; i++) {
            if (this.gameState.teams[i] === "re") {
                this.gameState.rePips += this.gameState.pips[i];
            }
            else if (this.gameState.teams[i] === "contra") {
                this.gameState.contraPips += this.gameState.pips[i];
            }
        }
        if (this.didReWin(this.gameState.rePips)) {
            this.gameState.winner = "re";
        }
        else if (this.didContraWin(this.gameState.contraPips)) {
            this.gameState.winner = "contra";
        }
        else {
            this.gameState.winner = "nobody";
        }
    }

    didReWin(rePips) {
        let a = this.gameState.announcements.map(entry => entry.name);
        if (a.includes("contraWillBeBlank")) {
            return rePips === 240;
        }
        if (a.includes("contraWillHaveThirtyOrLess")) {
            return rePips > 210;
        }
        if (a.includes("contraWillHaveSixtyOrLess")) {
            return rePips > 180;
        }
        if (a.includes("contraWillHaveNinetyOrLess")) {
            return rePips > 150;
        }

        if (a.includes("reWillBeBlank")) {
            return rePips > 0;
        }
        if (a.includes("reWillHaveThirtyOrLess")) {
            return rePips >= 30;
        }
        if (a.includes("reWillHaveSixtyOrLess")) {
            return rePips >= 60;
        }
        if (a.includes("reWillHaveNinetyOrLess")) {
            return rePips >= 90;
        }

        if ((!a.includes("re") && !a.includes("contra"))
            || a.includes("re")) {
            return rePips > 120;
        }
        if (a.includes("contra")) {
            return rePips >= 120;
        }

    }

    didContraWin(contraPips) {
        let a = this.gameState.announcements.map(entry => entry.name);
        if (a.includes("reWillBeBlank")) {
            return contraPips === 240;
        }
        if (a.includes("reWillHaveThirtyOrLess")) {
            return contraPips > 210;
        }
        if (a.includes("reWillHaveSixtyOrLess")) {
            return contraPips > 180;
        }
        if (a.includes("reWillHaveNinetyOrLess")) {
            return contraPips > 150;
        }

        if (a.includes("contraWillBeBlank")) {
            return contraPips > 0;
        }
        if (a.includes("contraWillHaveThirtyOrLess")) {
            return contraPips >= 30;
        }
        if (a.includes("contraWillHaveSixtyOrLess")) {
            return contraPips >= 60;
        }
        if (a.includes("contraWillHaveNinetyOrLess")) {
            return contraPips >= 90;
        }

        if ((!a.includes("re") && !a.includes("contra"))
            || a.includes("re")) {
            return contraPips >= 120;
        }
        if (a.includes("contra")) {
            return contraPips > 120;
        }
    }

    isMyTurn() { // human players turn
        return (0 === this.gameState.currentPlayerIndex);
    }

    getCurrentPlayer() {
        return this.players[this.gameState.currentPlayerIndex];
    }

    getCurrentPlayersHand() {
        return this.gameState.hands[this.gameState.currentPlayerIndex];
    }
}

export default DokoGame;
import React from 'react';

export function Player(props) {
  const announcements = props.announcements.map(e => <span key={e.name}>{e.name} </span>);
  return (
    <div className={"Player " + props.position}>{props.playerName}:
      <b> {props.team}</b>
      <span>{props.points}</span><br />
      {announcements}
    </div>
  );
}

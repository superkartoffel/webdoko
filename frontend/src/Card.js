import React from 'react';
import cards from './images/card-images';

export function Card(props) {
  let cardClasses = ["Card", props.value.shortName];
  if (props.className)
    cardClasses.push(props.className);
  if (!props.value.canDrawNow)
    cardClasses.push("disabled");

  return (
    <div
      className={cardClasses.join(" ")}>
       <img
        onClick={props.onClick}
        src={cards[props.value.shortName]}
        alt={props.value.shortName} /> 
    </div>
  );
}

import React from 'react';
import { useTranslation } from 'react-i18next';

export function AnnouncementButton ({onClick, announcements, newGame}) {
  const { t } = useTranslation();
  const [unfolded, setUnfolded] = React.useState(false);
  const announcementBtns = announcements.map((value) => <li key={value}><button onClick={(_e) => onClick(value)}>{t(value)}</button></li>);

  const handleClick = () => {
    setUnfolded(!unfolded);
  };

  if (announcementBtns.length > 0) {
    const showHideBtn = [<li key="0"><button onClick={handleClick}>{(!unfolded) ? t("show-announcements") : t("hide-announcements")}</button></li>];
    const caveatBtns = [
      <li key="healthy"><button onClick={(_e) => onClick("healthy")}>{t('healthy')}</button></li>,
      <li key="caveat"><button onClick={handleClick}>{t('caveat')}</button></li>
    ];
    const showBtns = (unfolded) ?
      [showHideBtn, announcementBtns] : (newGame ? caveatBtns : showHideBtn);

    return (
      <ul className="Announcements">
        {showBtns}
      </ul>
    );
  } else {
    return null;
  }
};

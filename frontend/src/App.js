import React, { Component } from 'react';
import { AnnouncementButton } from './AnnouncementButton';
import './App.css';
import { ComputerPlayer } from './ComputerPlayer';
import DokoGame from './DokoGame.js';
import { Hand } from './Hand';
import { Player } from './Player';
import { ResultTable } from './ResultTable';
import { Table } from './Table';
import './i18n';

class App extends Component {
  constructor() {
    super();
    this.db = null;
    this.gameResult = [];
    this.dokoGame = new DokoGame(this);
    this.dokoGame.setGameResults(this.gameResult);
    this.computerPlayer = new ComputerPlayer(this);
    this.dokoGame.players = [ 
      this,
      this.computerPlayer,
      this.computerPlayer,
      this.computerPlayer,
    ];
    this.state = { 
      gameState: this.dokoGame.gameState
    };

    this.handCardClickHandler = this.handCardClickHandler.bind(this);
    this.announcementsClickHandler = this.announcementsClickHandler.bind(this);
    this.updateState = this.updateState.bind(this);
    this.saveState = this.saveState.bind(this);
    this.loadState = this.loadState.bind(this);
    this.restartGame = this.restartGame.bind(this);
  }

  componentDidMount() {
    this.initdb();

    // let the computer players make their announcements
    for (let i=1;i<4;i++) {
      this.computerPlayer.myTurn(this.dokoGame, i);
    }
  }

  roundComplete() {
    return this.gameResult.length >= 24;
  }

  restartGame() {
    if (this.roundComplete()) {
      this.gameResult = [];
    }
    this.dokoGame = new DokoGame(this);
    this.dokoGame.setGameResults(this.gameResult);
    this.dokoGame.players = [ 
      this,
      this.computerPlayer,
      this.computerPlayer,
      this.computerPlayer,
    ];

    // let the computer players make their announcements
    for (let i=1;i<4;i++) {
      this.computerPlayer.myTurn(this.dokoGame, i);
    }

    this.updateState();
  }

  handCardClickHandler(id) {
    console.log(id);
    if (this.dokoGame.isMyTurn())
    {
      this.dokoGame.moveHandCardToTable(0, id);
    }
    this.setState({ gameState: this.dokoGame.gameState });
  }

  announcementsClickHandler(value) {
    console.log(value);
    if (value !== "") {
      this.dokoGame.makeAnnouncement(value, 0);
      this.setState({ gameState: this.dokoGame.gameState });
    }
  }

  updateState() {
    this.saveState();
    this.setState({ gameState: this.dokoGame.gameState });
  }

  myTurn() {
    console.log("Human players turn");
    this.setState({ gameState: this.dokoGame.gameState });
  }

  render() {
    let bNewGame = this.state.gameState.announcements.filter((entry) => entry.player === 0).length === 0;
    const announcementsByPlayer = [0,1,2,3].map(i => this.state.gameState.announcements.filter(e => e.player === i));

    // remove healthy and solo announcements that are not beeing played
    announcementsByPlayer.forEach(e => {
      if (e.length > 0 && (e[0].name === "healthy" || e[0].name !== this.state.gameState.gameMode)) {
        e.splice(0, 1);
      }
    });

    return (
      <div>
        <Table cards={this.state.gameState.currentTrick.cards} />
        <Player position="Bottom" playerName="Player" points={this.state.gameState.pips[0]} team={this.state.gameState.teams[0]} announcements={announcementsByPlayer[0]} />
        <Player position="Left" playerName="Bot 1" points={this.state.gameState.pips[1]} team={this.state.gameState.teams[1]} announcements={announcementsByPlayer[1]} />
        <Player position="Top" playerName="Bot 2" points={this.state.gameState.pips[2]} team={this.state.gameState.teams[2]} announcements={announcementsByPlayer[2]} />
        <Player position="Right" playerName="Bot 3" points={this.state.gameState.pips[3]} team={this.state.gameState.teams[3]} announcements={announcementsByPlayer[3]} />
        <Hand cards={this.state.gameState.hands[0]} onClick={this.handCardClickHandler} />
        <AnnouncementButton 
          newGame={bNewGame}
          announcements={this.state.gameState.possibleAnnouncements[0]} 
          onClick={this.announcementsClickHandler} />
        <ResultTable 
          winner={this.state.gameState.pointReceiver} 
          value={this.state.gameState.value} 
          points={this.state.gameState.points} 
          restartGame={this.restartGame}
          rePips={this.state.gameState.rePips}
          contraPips={this.state.gameState.contraPips}
          allResults={this.gameResult} />
      </div>
    ); 
  }

  initdb() {
    if (!window.indexedDB) {
      window.alert("Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.");
    } else {
      var request = window.indexedDB.open("gameStateDB", 4);

      request.onerror = function(event) {
        // Handle errors.
        alert("open failed.");
      };
      request.onupgradeneeded = (event) => {
        var db = event.target.result;

        if (!db.objectStoreNames.contains("webdoko")) {
          var objectStore = db.createObjectStore("webdoko", { keyPath: "id" });
          objectStore.transaction.oncomplete = (event) => {
            this.saveState();
          };
        }
        else {
          objectStore = event.target.transaction.objectStore("webdoko");
          var objectStoreRequest = objectStore.clear();
        
          objectStoreRequest.onsuccess = (event) => {
            this.saveState();
          };
        }

      };
      var app = this;
      request.onsuccess = function(event) {
        app.db = event.target.result;
        app.loadState();
      };
    }
  }

  loadState() {
    if (this.db) {
      var objectStore = this.db.transaction(["webdoko"], "readwrite").objectStore("webdoko");
      
      var request = objectStore.get(0);
      request.onerror = function(event) {
        // Handle errors!
        alert("load failed");
      };
      var app = this;
      request.onsuccess = function(event) {
        // Get the old value that we want to update
        if (event.target.result) {
          app.dokoGame.gameState = event.target.result.data;
          app.updateState();
        }
      };

      var request2 = objectStore.get(1);
      request2.onerror = function(event) {
        // Handle errors!
        alert("load failed");
      };
      request2.onsuccess = function(event) {
        // Get the old value that we want to update
        if (event.target.result) {
          app.gameResult = event.target.result.data;
          app.dokoGame.setGameResults(app.gameResult);
          app.updateState();
        }
      };
    }
  }

  saveState() {
    if (this.db) {
      var objectStore = this.db.transaction(["webdoko"], "readwrite").objectStore("webdoko");
      // Put this updated object back into the database.
      var requestUpdate = objectStore.put({id: 0, data: this.dokoGame.gameState});
      requestUpdate.onerror = function(event) {
        // Do something with the error
        alert("save failed");
      };
      requestUpdate.onsuccess = function(event) {
        // Success - the data is updated!
      };

      var requestUpdate2 = objectStore.put({id: 1, data: this.gameResult});
      requestUpdate2.onerror = function(event) {
        // Do something with the error
        alert("save failed");
      };
      requestUpdate2.onsuccess = function(event) {
        // Success - the data is updated!
      };
    }
  }
}

export default App;
import React from 'react';
import { Card } from './Card';

export function Table(props) {
  const positions = {
    0: "BottomCard",
    1: "LeftCard",
    2: "TopCard",
    3: "RightCard",
  };
  const cards = props.cards.map((value, index) => <Card value={value} key={value.id} className={positions[index]} />);
  return (
    <div className="Table">
      {cards}
    </div>
  );
}

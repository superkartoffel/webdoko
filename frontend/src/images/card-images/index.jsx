// const reqSvgs = require.context ('./', true, /\.svg$/ )

// const cards = reqSvgs
//     .keys ()
//     .reduce ( ( images, path ) => {
//     images[path] = reqSvgs ( path )
//     return images
//     }, {} );

// export default cards;

import c10 from './c10.svg';
import cA from './cA.svg';
import cJ from './cJ.svg';
import cK from './cK.svg';
import cQ from './cQ.svg';
import d10 from './d10.svg';
import dA from  './dA.svg';
import dJ from  './dJ.svg';
import dK from  './dK.svg';
import dQ from  './dQ.svg';
import h10 from './h10.svg';
import hA from  './hA.svg';
import hJ from  './hJ.svg';
import hK from  './hK.svg';
import hQ from  './hQ.svg';
import s10 from './s10.svg';
import sA from  './sA.svg';
import sJ from  './sJ.svg';
import sK from  './sK.svg';
import sQ from  './sQ.svg';

const cards = {
    c10, cA, cJ, cK, cQ,
    d10, dA, dJ, dK, dQ,
    h10, hA, hJ, hK, hQ,
    s10, sA, sJ, sK, sQ,
};
export default cards;
import React from 'react';
import { useTranslation } from 'react-i18next';

export function ResultTable(props) {
  const { t } = useTranslation();
  if (props.value !== null) {
    const gameModeSymbols = {
      normal: "",
      marriage: "⚭",
      jackSolo: t("J"),
      queenSolo: t("Q"),
      spadesSolo: "♠",
      heartsSolo: "♥",
      diamondsSolo: "♦",
      clubsSolo: "♣",
      meatlessSolo: t("A"),
    };

    const results = props.points.map((entry, i) => <tr key={i}><td>{entry.points}</td><td>{t(entry.text)}</td></tr>);

    const allResults = props.allResults.map((e, i) => <tr key={i}>
      <td>{e.player1points}</td>
      <td>{e.player2points}</td>
      <td>{e.player3points}</td>
      <td>{e.player4points}</td>
      <td>{gameModeSymbols[e.gameMode]}{e.obligatorySoloPlayer !== null ? "*" : ""}</td>
    </tr>);

    let resultSums = [0, 0, 0, 0];
    for (let i = 0; i < props.allResults.length; i++) {
      resultSums[0] += props.allResults[i].player1points;
      resultSums[1] += props.allResults[i].player2points;
      resultSums[2] += props.allResults[i].player3points;
      resultSums[3] += props.allResults[i].player4points;
    }

    const tableResultSums = (
      <tr>
        <td><b>&sum; {resultSums[0]}</b></td>
        <td><b>&sum; {resultSums[1]}</b></td>
        <td><b>&sum; {resultSums[2]}</b></td>
        <td><b>&sum; {resultSums[3]}</b></td>
        <td></td>
      </tr>
    );
    return (
      <div>
        <div className="ResultTable">
          <div className="ResultTableInner">
            <table>
              <tbody>
                <tr><th>Re:</th><th>Contra:</th></tr>
                <tr><td>{props.rePips} {t('pips')}</td><td>{props.contraPips} {t('pips')}</td></tr>
              </tbody>
            </table>
            <table>
              <tbody>
                <tr><th>{t('points')}</th><th>{t('reason')}</th></tr>
                {results}
                <tr><td><b>&sum; {props.value}</b></td><td>{t('for')} {props.winner}</td></tr>
              </tbody>
            </table>
            {t('All games results')}:
            <table>
              <tbody>
                <tr><th>Player 1</th><th>Player 2</th><th>Player 3</th><th>Player 4</th><th>Solo</th></tr>
                {allResults}
                {tableResultSums}
              </tbody>
            </table>
          </div>
        </div>
        <button onClick={props.restartGame} className="continueButton">{t('Next game')}</button>
      </div>);
  }
  return null;
}

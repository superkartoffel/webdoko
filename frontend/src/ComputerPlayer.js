export class ComputerPlayer {
  constructor(app) {
    this.app = app;
  }

  myTurn(dokoGame, playerPosition) {
    // pick a random card thats playable
    let hand = dokoGame.gameState.hands[playerPosition];
    let playableCards = hand.filter((card) => { return card.canDrawNow; });
    let playableCardNames = [];
    for (let c of playableCards) { playableCardNames.push(c.displayName); }
    console.log("Player " + playerPosition + " playable cards: " + playableCardNames);
    if (playableCards.length > 0) {
      let i = Math.floor(Math.random() * playableCards.length);
      let randomCard = playableCards[i];
      setTimeout(() => {
        console.log("Playing card " + i + randomCard);
        dokoGame.moveHandCardToTable(playerPosition, randomCard.id);
        this.app.updateState();
      }, 300);
    }
    else {
      // lets see if we should player a marriage
      if (dokoGame.gameState.possibleAnnouncements[playerPosition].includes("marriage")) {
        dokoGame.makeAnnouncement("marriage", playerPosition);
      }
      else if (dokoGame.gameState.possibleAnnouncements[playerPosition].includes("healthy")) {
        dokoGame.makeAnnouncement("healthy", playerPosition);
      }
    }
  }
}

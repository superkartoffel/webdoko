import React from 'react';
import { Card } from './Card';

export function Hand(props) {
  const cards = props.cards.map((value) => <Card value={value} key={value.id} onClick={(_e) => props.onClick(value.id)} />);
  return (
    <div className="Hand">
      <div className="HandInner">
        {cards}
      </div>
    </div>
  );
}
